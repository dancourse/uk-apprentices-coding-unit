# Apprentice Coding Unit 1010

If web coding was a unit on its own we reckon it should look something like this...

*This is a draft and VERY open to editing*

## Coding basics
  
Take simple coding languages and learn the basics in
* PHP
* HTML
* JS
* MYSQL

## Source Control
* Branching
* Remote working
* Branches

## Specalist subject
Every company has had different technology / technique they would like their apprentices to learn. And having a day of them following a tutorial with an experienced person has been very useful.

eg, learning Google Android Apps, learning NodeJs

## Agile Project management
* ...

## Servers
* Apache
* NodeJS

## Coding Theory

* Object Oriented Coding
* Principles of Solid
* Model View Controller
* Testing (Unit testing / Front end tests)
* Design patterns

## Advanced coding techniques

* NoSQL (MongoDb)
* CSS pre-proccessors (SASS / LESS)
* Server Frameworks: Zend Framework / Symphony / Ruby on rails / Umbraco
* ORM's (Doctrine)
* Fast Coding techniques
* Code Validation

